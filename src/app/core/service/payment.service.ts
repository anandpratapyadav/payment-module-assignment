import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Creditcard } from "src/app/shared/models/creditcard.model";

@Injectable({
  providedIn: "root",
})
export class PaymentService {
  constructor(private http: HttpClient) {}

  public makePayment(data): Observable<Creditcard> {
    const url = `https://5a158cca00caf30012274d10.mockapi.io/payment`;
    return this.http.post<Creditcard>(url, data);
  }
}
