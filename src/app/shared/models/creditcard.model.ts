export class Creditcard {
  creditCardNumber: number;
  cardholder: string;
  expirationDate: Date;
  securityCode: string;
  amount: number;
}
