import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from "@angular/material-moment-adapter";
import {
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
} from "@angular/material/core";
import * as moment from "moment";
import { Moment } from "moment";
import { BehaviorSubject } from "rxjs";
import { PaymentService } from "../core/service/payment.service";
export const MY_FORMATS = {
  parse: {
    dateInput: "MM/YYYY",
  },
  display: {
    dateInput: "MM/YYYY",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY",
  },
};
@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PaymentComponent implements OnInit {
  paymentForm: FormGroup;
  minDate: Moment;
  paymentList: any = [];
  loading$ = new BehaviorSubject<boolean>(false);
  constructor(
    private formBuilder: FormBuilder,
    private paymentService: PaymentService
  ) {}

  ngOnInit(): void {
    this.paymentForm = this.formBuilder.group({
      creditCardNumber: [
        null,
        [Validators.required, Validators.minLength(12), this.cardValidator()],
      ],
      cardholder: [null, Validators.required],
      expirationDate: [moment(), Validators.required],
      securityCode: [null, [Validators.minLength(3), Validators.maxLength(4)]],
      amount: [null, [Validators.required, Validators.min(1)]],
    });
  }
  ngDestroy(): void {
    this.loading$.unsubscribe();
  }

  myDateFilter = (m: Moment | null): boolean => {
    const year = (m || moment()).year();
    const month = (m || moment()).month();
    const date = (m || moment()).date();
    const res =
      year >= moment().year() &&
      month >= moment().month() &&
      date >= moment().date();
    return res;
  };
  submit() {
    if (this.paymentForm.invalid) {
      return;
    }
    this.loading$.next(true);
    this.paymentService.makePayment(this.paymentForm.value).subscribe((res) => {
      this.paymentList.push(res);
      this.loading$.next(false);
    });
  }

  cardValidator(): ValidatorFn {
    return (control: AbstractControl) => {
      const isValid = this.checkCardValoidation(control.value);
      return isValid ? null : { luhnCheck: isValid };
    };
  }

  checkCardValoidation(cardNumber) {
    if (cardNumber === null || !cardNumber.length) {
      return;
    }
    cardNumber = cardNumber.replace(/\s/g, "");
    const lastDigit = Number(cardNumber[cardNumber.length - 1]);
    const reverseCardNumber = cardNumber
      .slice(0, cardNumber.length - 1)
      .split("")
      .reverse()
      .map((x) => Number(x));

    let sum = 0;

    for (let i = 0; i <= reverseCardNumber.length - 1; i += 2) {
      reverseCardNumber[i] = reverseCardNumber[i] * 2;
      if (reverseCardNumber[i] > 9) {
        reverseCardNumber[i] = reverseCardNumber[i] - 9;
      }
    }
    sum = reverseCardNumber.reduce((acc, currValue) => acc + currValue, 0);
    return (sum + lastDigit) % 10 === 0;
  }
}
